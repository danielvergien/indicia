﻿-- View: list_survey_attribute_values

-- DROP VIEW list_survey_attribute_values;

CREATE OR REPLACE VIEW list_survey_attribute_values AS 
 SELECT sav.id, s.id AS survey_id, s.id AS survey_attribute_id, 
        CASE s.data_type
            WHEN 'T'::bpchar THEN 'Text'::bpchar
            WHEN 'L'::bpchar THEN 'Lookup List'::bpchar
            WHEN 'I'::bpchar THEN 'Integer'::bpchar
            WHEN 'B'::bpchar THEN 'Boolean'::bpchar
            WHEN 'F'::bpchar THEN 'Float'::bpchar
            WHEN 'D'::bpchar THEN 'Specific Date'::bpchar
            WHEN 'V'::bpchar THEN 'Vague Date'::bpchar
            ELSE s.data_type
        END AS data_type, s.caption, 
        CASE s.data_type
            WHEN 'T'::bpchar THEN sav.text_value
            WHEN 'L'::bpchar THEN t.term::text
            WHEN 'I'::bpchar THEN sav.int_value::character varying::text
            WHEN 'B'::bpchar THEN sav.int_value::character varying::text
            WHEN 'F'::bpchar THEN sav.float_value::character varying::text
            WHEN 'D'::bpchar THEN sav.date_start_value::character varying::text
            WHEN 'V'::bpchar THEN (sav.date_start_value::character varying::text || ' - '::text) || sav.date_end_value::character varying::text
            ELSE NULL::text
        END AS value, 
        CASE s.data_type
            WHEN 'T'::bpchar THEN sav.text_value
            WHEN 'L'::bpchar THEN sav.int_value::character varying::text
            WHEN 'I'::bpchar THEN sav.int_value::character varying::text
            WHEN 'B'::bpchar THEN sav.int_value::character varying::text
            WHEN 'F'::bpchar THEN sav.float_value::character varying::text
            WHEN 'D'::bpchar THEN sav.date_start_value::character varying::text
            WHEN 'V'::bpchar THEN (sav.date_start_value::character varying::text || ' - '::text) || sav.date_end_value::character varying::text
            ELSE NULL::text
        END AS raw_value, s.termlist_id, l.iso, saw.website_id
   FROM surveys s
   LEFT JOIN survey_attribute_values sav ON sav.survey_id=s.id AND sav.deleted=false
   LEFT JOIN (users u
     JOIN users_websites uw ON uw.user_id = u.id AND uw.site_role_id is not null 
     JOIN survey_attributes_websites saw ON saw.website_id = uw.website_id AND saw.deleted = false
   ) ON u.survey_id=s.id
   JOIN survey_attributes sa ON (s.id=COALESCE(sav.survey_attribute_id, saw.survey_attribute_id) 
       OR s.public=true) AND (s.id = sav.survey_attribute_id OR sav.id IS NULL) AND s.deleted=false
   LEFT JOIN (termlists_terms tt
   JOIN terms t ON t.id = tt.term_id
   JOIN languages l ON l.id = t.language_id) ON tt.meaning_id = sav.int_value AND s.data_type = 'L'::bpchar
  WHERE s.deleted = false
  ORDER BY s.id;
