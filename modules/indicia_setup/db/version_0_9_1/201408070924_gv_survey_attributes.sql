﻿-- View: gv_survey_attributes

-- DROP VIEW gv_survey_attributes;

CREATE OR REPLACE VIEW gv_survey_attributes AS 
         SELECT sa.id, oaw.website_id, w.title AS website, sa.caption, 
                CASE sa.data_type
                    WHEN 'T'::bpchar THEN 'Text'::bpchar
                    WHEN 'L'::bpchar THEN 'Lookup List'::bpchar
                    WHEN 'I'::bpchar THEN 'Integer'::bpchar
                    WHEN 'B'::bpchar THEN 'Boolean'::bpchar
                    WHEN 'F'::bpchar THEN 'Float'::bpchar
                    WHEN 'D'::bpchar THEN 'Specific Date'::bpchar
                    WHEN 'V'::bpchar THEN 'Vague Date'::bpchar
                    ELSE sa.data_type
                END AS data_type, sa.public, sa.created_by_id, sa.deleted
           FROM survey_attributes sa
      LEFT JOIN survey_attributes_websites oaw ON sa.id = oaw.survey_attribute_id AND oaw.deleted = false
   LEFT JOIN websites w ON w.id = oaw.website_id AND w.deleted = false
  WHERE sa.deleted = false;

